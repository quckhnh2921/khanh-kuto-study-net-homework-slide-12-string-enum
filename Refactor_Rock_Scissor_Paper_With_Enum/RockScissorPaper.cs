﻿namespace RockScissorPaperNameSpace{
    public class RockScissorPaper
    {
        public static int player1Choose;
        public static int player2Choose;
        public static int userChoice;
        public static int countPlayer1Win = 0;
        public static int countPlayer2Win = 0;
        public static string PLAYER_1_WIN = "Player 1 win !";
        public static string PLAYER_2_WIN = "Player 2 win !";
        enum Choice
        {
            KEO = 1,
            BUA = 2,
            BAO = 3
        }
        public void MainMenu()
        {
            Console.WriteLine("-------MENU-------");
            Console.WriteLine("| 1: Play game   |");
            Console.WriteLine("| 2: View History|");
            Console.WriteLine("| 3: Exit !      |");
            Console.WriteLine("------------------");
            Console.Write("Choice: ");
            try
            {
                userChoice = int.Parse(Console.ReadLine());
                if (userChoice < 1 || userChoice > 3)
                {
                    throw new Exception("Choice is out of range 1-3");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        private void GameMenu()
        {
            Console.WriteLine("--------------");
            Console.WriteLine("|  1. KEO    |");
            Console.WriteLine("|  2. BUA    |");
            Console.WriteLine("|  3. BAO    |");
            Console.WriteLine("--------------");
        }
        public void UserInput()
        {
            try
            {
                GameMenu();
                Console.Write("Enter player 1 choose: ");
                player1Choose = int.Parse(Console.ReadLine());
                Console.Write("Enter player 2 choose: ");
                player2Choose = int.Parse(Console.ReadLine());
                if (player1Choose < 1 || player1Choose > 3 || player2Choose < 1 || player2Choose > 3)
                {
                    throw new Exception("Player choice is out of range 1-3 !");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void DisplayGraphic(int player1Choose, int player2Choose)
        {
            try
            {
                if (player1Choose < 1 || player1Choose > 3 || player2Choose < 1 || player2Choose > 3)
                {
                    throw new Exception("");
                }
                if (player1Choose == (int)Choice.KEO)
                {
                    Console.Write("✂️");
                }
                else if (player1Choose == (int)Choice.BUA)
                {
                    Console.Write("🔨");
                }
                else if (player1Choose == (int)Choice.BAO)
                {
                    Console.Write("🕸️");
                }
                Console.Write("💥");
                if (player2Choose == (int)Choice.KEO)
                {
                    Console.WriteLine("✂️");
                }
                else if (player2Choose == (int)Choice.BUA)
                {
                    Console.WriteLine("🔨");
                }
                else if (player2Choose == (int)Choice.BAO)
                {
                    Console.WriteLine("🕸️");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        public string GameResult(int player1Choose, int player2Choose)
        {
            bool player1Win = player1Choose == (int)Choice.KEO && player2Choose == (int)Choice.BAO
                            || player1Choose == (int)Choice.BUA && player2Choose == (int)Choice.KEO
                            || player1Choose == (int)Choice.BAO && player2Choose == (int)Choice.BUA;
            bool player2Win = player2Choose == (int)Choice.KEO && player1Choose == (int)Choice.BAO
                            || player2Choose == (int)Choice.BUA && player1Choose == (int)Choice.KEO
                            || player2Choose == (int)Choice.BAO && player1Choose == (int)Choice.BUA;
            bool draw = player2Choose == (int)Choice.KEO && player1Choose == (int)Choice.KEO
                           || player2Choose == (int)Choice.BUA && player1Choose == (int)Choice.BUA
                           || player2Choose == (int)Choice.BAO && player1Choose == (int)Choice.BAO;
            if (player1Win)
            {
                countPlayer1Win++;
                return PLAYER_1_WIN;
            }
            if (player2Win)
            {
                countPlayer2Win++;
                return PLAYER_2_WIN;
            }
            if (draw)
            {
                return "DRAW !";
            }
            return "Invalid";
        }

    }
}
