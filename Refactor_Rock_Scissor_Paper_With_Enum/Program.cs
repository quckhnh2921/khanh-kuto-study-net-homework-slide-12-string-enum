﻿using RockScissorPaperNameSpace;
using System.Linq.Expressions;
using System.Xml.Serialization;

Console.OutputEncoding = System.Text.Encoding.Default;
RockScissorPaper game = new RockScissorPaper();
WriteAndReadFile file = new WriteAndReadFile();
do
{
    try
    {
        game.MainMenu();
        switch (RockScissorPaper.userChoice)
        {
            case 1:
                game.UserInput();
                game.DisplayGraphic(RockScissorPaper.player1Choose, RockScissorPaper.player2Choose);
                var result = game.GameResult(RockScissorPaper.player1Choose, RockScissorPaper.player2Choose);
                Console.WriteLine(result);
                file.WriteGameHistory(result);
                break;
            case 2:
                file.ReadGameHistoryPlayerOne();
                file.ReadGameHistoryPlayerTwo();
                break;
            case 3:
                Console.WriteLine("BYE BYE! ");
                Environment.Exit(0);
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (RockScissorPaper.userChoice != 3);


