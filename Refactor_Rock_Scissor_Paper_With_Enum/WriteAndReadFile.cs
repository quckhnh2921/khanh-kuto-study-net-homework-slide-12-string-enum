﻿namespace RockScissorPaperNameSpace
{
    class WriteAndReadFile
    {
        public string player1FilePath = @"D:\Study process\C# basic study\C#_Basic_Homework_Slide_12_String_And_Enum\C#_Basic_Homework_Slide_12_String_And_Enum\Refactor_Rock_Scissor_Paper_With_Enum\Player1BestScore.txt";
        public string player2FilePath = @"D:\Study process\C# basic study\C#_Basic_Homework_Slide_12_String_And_Enum\C#_Basic_Homework_Slide_12_String_And_Enum\Refactor_Rock_Scissor_Paper_With_Enum\Player2BestScore.txt";
        public void WriteGameHistory(string result)
        {
            string player1History = player1FilePath;
            string player2History = player2FilePath;
            string content = "";
            if (File.Exists(player1History))
            {
                if (result.Equals(RockScissorPaper.PLAYER_1_WIN))
                {
                    content = "Player 1 best score: " + RockScissorPaper.countPlayer1Win;
                    File.WriteAllText(player1History, content);
                    Console.WriteLine("History save success !");
                }
                if (result == RockScissorPaper.PLAYER_2_WIN)
                {
                    content = "Player 2 best score: " + RockScissorPaper.countPlayer2Win;
                    File.WriteAllText(player2History, content);
                    Console.WriteLine("History save success !");
                }
            }
            else
            {
                throw new Exception("Saving fail !");
            }
        }
        public void ReadGameHistoryPlayerOne()
        {
            string filePath = player1FilePath;

            if (File.Exists(filePath))
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                    }
                }
            }
            else
            {
                throw new Exception("Can not read Player 1 History !");
            }
        }
        public void ReadGameHistoryPlayerTwo()
        {
            string filePath = player2FilePath;

            if (File.Exists(filePath))
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                    }
                }
            }
            else
            {
                throw new Exception("Can not read Player 2 History !");
            }
        }

    }
}
