﻿class WriteAndReadFile
{
    string filePath = @"D:\Study process\C# basic study\C#_Basic_Homework_Slide_12_String_And_Enum\C#_Basic_Homework_Slide_12_String_And_Enum\ReadDocument\Document.txt";
    public int countLine = 0;
    public void ReadFile()
    {
        string path = filePath; 
        if (File.Exists(path))
        {
            using (StreamReader reader = new StreamReader(path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                    countLine++;
                }
            }
        }
        else
        {
            throw new Exception("Can not read document!");
        }
    }
}