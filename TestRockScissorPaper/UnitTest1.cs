﻿namespace RockScissorPaperNameSpace
{
    public class UnitTest1
    {
        [Fact]
        public void GameResult_ShouldReturnPlayer1Win_WhenParamIs1AndParamIs3()
        {
            RockScissorPaper game = new RockScissorPaper();
            var param = 1;
            var param2 = 3;
            var expected = "Player 1 win !";
            var atual = game.GameResult(param, param2);
            Assert.Equal(expected, atual);
        }
        [Fact]
        public void GameResult_ShouldReturnPlayer1Win_WhenParamIs2AndParamIs1()
        {
            RockScissorPaper game = new RockScissorPaper();
            var param = 2;
            var param2 = 1;
            var expected = "Player 1 win !";
            var atual = game.GameResult(param, param2);
            Assert.Equal(expected, atual);
        }
        [Fact]
        public void GameResult_ShouldReturnPlayer1Win_WhenParamIs3AndParamIs2()
        {
            RockScissorPaper game = new RockScissorPaper();
            var param = 3;
            var param2 = 2;
            var expected = "Player 1 win !";
            var atual = game.GameResult(param, param2);
            Assert.Equal(expected, atual);
        }
        [Fact]
        public void GameResult_ShouldReturnPlayer2Win_WhenParamIs1AndParamIs2()
        {
            RockScissorPaper game = new RockScissorPaper();
            var param = 1;
            var param2 = 2;
            var expected = "Player 2 win !";
            var atual = game.GameResult(param, param2);
            Assert.Equal(expected, atual);
        }
        [Fact]
        public void GameResult_ShouldReturnPlayer2Win_WhenParamIs2AndParamIs3()
        {
            RockScissorPaper game = new RockScissorPaper();
            var param = 1;
            var param2 = 2;
            var expected = "Player 2 win !";
            var atual = game.GameResult(param, param2);
            Assert.Equal(expected, atual);
        }
        [Fact]
        public void GameResult_ShouldReturnPlayer2Win_WhenParamIs3AndParamIs1()
        {
            RockScissorPaper game = new RockScissorPaper();
            var param = 3;  
            var param2 = 1;
            var expected = "Player 2 win !";
            var atual = game.GameResult(param, param2);
            Assert.Equal(expected, atual);
        }
        [Fact]
        public void GameResult_ShouldReturnDraw_WhenParamIs1AndParam2Is1()
        {
            RockScissorPaper game = new RockScissorPaper();
            var param = 1;
            var param2 = 1;
            var expected = "DRAW !";
            var atual = game.GameResult(param, param2);
            Assert.Equal(expected, atual);
        }
        [Fact]
        public void GameResult_ShouldReturnDraw_WhenParamIs2AndParam2Is2()
        {
            RockScissorPaper game = new RockScissorPaper();
            var param = 2;
            var param2 = 2;
            var expected = "DRAW !";
            var atual = game.GameResult(param, param2);
            Assert.Equal(expected, atual);
        }
        [Fact]
        public void GameResult_ShouldReturnDraw_WhenParamIs3AndParam2Is3()
        {
            RockScissorPaper game = new RockScissorPaper();
            var param = 3;
            var param2 = 3;
            var expected = "DRAW !";
            var atual = game.GameResult(param, param2);
            Assert.Equal(expected, atual);
        }
    }
}